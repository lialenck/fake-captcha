import * as Vue from 'vue'
import App from './App.vue'

export let baseUrl = "http://localhost:20080/"

export function delay(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, ms));
}

Vue.createApp(App).mount('#app')
