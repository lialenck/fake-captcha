import Ajv, { JSONSchemaType, DefinedError } from "ajv"
import YAML from "yaml"
import fs from 'fs'

type FixedSizeArray<N extends number, T> = N extends 0 ? never[] : {
    0: T;
    length: N;
} & ReadonlyArray<T>;

export type Config = (CaptchaBigImg | CaptchaSmallImgs)[]

export interface CaptchaBigImg {
    type: "BigImg",
	instructions: string,
	solutions: FixedSizeArray<16, boolean | null>,
    img: string,
	action: Action,
}

export interface CaptchaSmallImgs {
    type: "SmallImgs",
	instructions: string,
	// would love to use [string, ...string[]] for the next field, but idk how
	// to make that work with AJV
    imgs: FixedSizeArray<9, string[]>,
	action: Action,
}

export enum ActionType {
    next = "next",
    toIndex = "toIndex",
    end = "end",
}

export interface Action {
    type: ActionType,
    index?: number
}

// TODO if type is toIndex, index shall not be nullable (and otherwise it must be null)
const actionSchema: JSONSchemaType<Action> = {
	type: "object",
	required: ["type"],
	additionalProperties: false,
	properties: {
		type: { type: "string", enum: [ActionType.next, ActionType.toIndex, ActionType.end] },
		index: { type: "integer", nullable: true },
	}
}

const bigImgSchema: JSONSchemaType<CaptchaBigImg> = {
	type: "object",
	required: ["type", "solutions", "img", "action"],
	additionalProperties: false,
	properties: {
		type: { type: "string", const: "BigImg" },
		instructions: { type: "string" },
		solutions: {
			type: "array",
			minItems: 16,
			maxItems: 16,
			items: {
				type: "boolean",
				nullable: true
			},
		},
		img: { type: "string" },
		action: actionSchema,
	}
}

const smallImgsSchema: JSONSchemaType<CaptchaSmallImgs> = {
	type: "object",
	required: ["type", "imgs", "action"],
	additionalProperties: false,
	properties: {
		type: { type: "string", const: "SmallImgs" },
		instructions: { type: "string" },
		imgs: {
			type: "array",
			minItems: 9,
			maxItems: 9,
			items: {
				type: "array",
				minItems: 1,
				items: {
					type: "string",
					nullable: false,
				},
			},
		},
		action: actionSchema,
	}
}

const configSchema: JSONSchemaType<Config> = {
	type: "array",
	items: {
		type: "object",
		anyOf: [bigImgSchema, smallImgsSchema]
	}
}

const ajv = new Ajv()
const validateConfig = ajv.compile<Config>(configSchema)

export function loadConfig(file: string): Config | null {
	let captchas = YAML.parse(fs.readFileSync(file, "utf-8"))
	if (validateConfig(captchas)) {
		// theres one thing I wasn't able to validate using AJV:
		// whether all the indices for the ActionType are valid.
		// So, we do that manually

		for (let i in captchas) {
			let a = captchas[i].action

			if (a.type != "toIndex") {
				// may not have index if type isn't toIndex
				if (a.index != undefined) return null

				// also, "next" can't be at the end
				if (a.type == "next" && parseInt(i) == captchas.length - 1) return null
			}
			else if (!Number.isInteger(a.index)
				  || (a.index as number) < 0
				  || (a.index as number) >= captchas.length) {
				// index must be integer and refer to a captcha
				return null
			}
		}

		return captchas
	}
	else {
		console.error("in " + file + ": type doesn't match schema:")
		for (let err of (validateConfig.errors as DefinedError[])) {
			console.error("\t" + JSON.stringify(err))
		}
		return null
	}
}
