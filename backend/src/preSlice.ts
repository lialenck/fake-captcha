import fs from "fs"
import path from "path"
import ImageSize from "image-size"
import Sharp from "sharp"
const imageToSlices = require("image-to-slices")

import * as Cfg from "./config"

function getWebPImageSlices(path: string): Promise<string[]> {
	let imagePath = "assets/" + path
    return new Promise((resolve, _) => {
        let dims = ImageSize(imagePath)
        let smaller = Math.min(dims.height || 0, dims.width || 0)
        let size = Math.floor(smaller / 4)
        let lineArr = [size, 2*size, 3*size]
    
        let results: Buffer[] = []
        let promises: Promise<string>[] = []
    
        imageToSlices(imagePath, lineArr, lineArr, {
            saveToDataUrl: true,
            clipperOptions: {
                canvas: require('canvas')
            }
        }, async (urlList:any) => {
            for (let img of urlList) {
				let imgBuf = Buffer.from(img.dataURI.split(';base64,').pop(), 'base64')
				promises.push(Sharp(imgBuf).webp().toBuffer().then(buf => buf.toString('base64')))
            }
            resolve(await Promise.all(promises))
        })
    })
}

async function preSliceImage(file: string): Promise<boolean> {
	try {
		console.log("trying to pre-slice " + file)
		let data = await getWebPImageSlices(file)
		let out_path = path.join(__dirname, "../assets/sliced/", file + ".json")
		await new Promise<void>((resolve, reject) => {
			fs.writeFile(out_path, JSON.stringify(data), err => { reject(err); })
			resolve()
		})
		console.log("written to " + out_path)
		return true
	} catch (_) {
		return false
	}
}

async function main() {
	const config = Cfg.loadConfig("config.yaml")
	console.log(config)

	if (config == null) {
		console.error("Couldn't load config.")
		throw ":("
	}
	Promise.all(config.map(async captcha => {
		if (captcha.type == "BigImg") {
			if (!await preSliceImage(captcha.img)) {
				console.error("Failed to pre-slice " + captcha.img)
				throw ":("
			}
		}
	}))
}
main()
