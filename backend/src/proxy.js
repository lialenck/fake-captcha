let target = "codeberg.org"

let socks = require('socksv5')
let dns = require('dns')

function allFulfilled(ps) {
	return Promise.allSettled(ps).then(res => res.reduce((l, next) => {
		if (next.status == "fulfilled")
			l.push(next.value);
		return l;
	}, []))
}

function asyncResolveWithResolver(resolver, domain) {
	return new Promise((resolve, reject) => {
		resolver(domain, (err, ad) => {
			if (err != null) reject(err);
			else resolve(ad);
		})
	})
}
function asyncResolve4(domain) { return asyncResolveWithResolver(dns.resolve4, domain) }
function asyncResolve6(domain) { return asyncResolveWithResolver(dns.resolve6, domain) }

module.exports = () => {
	allFulfilled([asyncResolve4(target), asyncResolve6(target)]).then(targetIps => {
		targetIps = targetIps.flatMap(x => x) // monad join
		console.log("Got responses:")
		console.log(targetIps)
	
		let srv = socks.createServer((info, accept, deny) => {
			if (!targetIps.includes(info.dstAddr) && info.dstAddr != "codeberg.org") {
				accept()
				return
			}
	
			console.log("Found target ip: " + info.dstAddr)
			info.dstAddr = "127.0.0.1"
			info.dstPort = info.dstPort == 80? 20080 : 20443
			accept()
		})
		srv.listen(1080, '127.0.0.1', () => {
			console.log('SOCKS server listening on port 1080')
		})
	
		srv.useAuth(socks.auth.None())
	})
}
