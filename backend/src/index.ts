import Express from "express"
import cors from "cors"
import path from "path"
import fs from "fs"
import http from "http"
import https from "https"

import * as Cfg from "./config"

let runProxy = require("./proxy")
let qs = require('node:querystring')

async function main() {
	const config = Cfg.loadConfig("config.yaml")
	console.log(config)

	let [pk, cert] = ["key.pem", "cert.pem"]
		.map(f => fs.readFileSync(path.join(__dirname, "../", f)))

	const app = Express()

	app.get("/slice/:name", cors<Express.Request>(), async (req, res) => {
		res.sendFile(path.join(__dirname, "../assets/sliced/", req.params.name + ".json"))
	})
	app.get("/raw/:name", cors<Express.Request>(), async (req, res) => {
		res.sendFile(path.join(__dirname, "../assets/", req.params.name))
	})
	app.get("/config.json", cors<Express.Request>(), async (req, res) => {
		res.send(config)
	})
	app.use(Express.static("frontend-dist"))
	app.get("*", async (req, res) => {
		console.log(req.url)
		res.redirect("/?return=" + qs.escape(req.url))
	})

	http.createServer(app).listen(20080)
	https.createServer({key: pk, cert: cert}, app).listen(20443)

	runProxy()
}
main()
